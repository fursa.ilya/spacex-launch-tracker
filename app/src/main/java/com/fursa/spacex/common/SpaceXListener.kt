package com.fursa.spacex.common

interface SpaceXListener {
    fun onEventsLoaded()
}