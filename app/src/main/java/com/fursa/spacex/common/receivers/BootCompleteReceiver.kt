package com.fursa.spacex.common.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import com.fursa.spacex.AppDelegate
import javax.inject.Inject

class BootCompleteReceiver : BroadcastReceiver() {

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var settings: SharedPreferences

    private val justBeforeKey = settings.getBoolean("just_before_key", false)
    private val dayBeforeKey = settings.getBoolean("day_before_key", false)


    override fun onReceive(ctx: Context?, intent: Intent?) {
        if(Intent.ACTION_BOOT_COMPLETED == intent?.action) {
            Log.d("BootCompleteReceiver", "Boot completed!")

            if(justBeforeKey) {
                Log.d("BootCompleteReceiver", "Just before option enabled")
            }

            if(dayBeforeKey) {
                Log.d("BootCompleteReceiver", "Day before option enabled")
            }
        }
    }
}