package com.fursa.spacex.common

enum class Month(val monthName: String) {
    JAN("Янв."),
    FEB("Февр."),
    MAR("Март."),
    APR("Апр."),
    MAY("Май."),
    JUN("Июн."),
    JUL("Июл."),
    AUG("Авг."),
    SEP("Сент."),
    OCT("Окт."),
    NOV("Нояб."),
    DEC("Дек."),
}