@file:JvmName("DateUtils")

package com.fursa.spacex.common

import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat

fun formatDate(stamp: Long?): String {
    val dateFormat = SimpleDateFormat("dd MMMM yyyy HH:mm")
    return dateFormat.format(Timestamp(stamp?.times(1000)!!))
}

fun formatUnknownDate(stamp: Long?): String {
    val dateFormat = SimpleDateFormat("MMMM yyyy")
    return dateFormat.format(Timestamp(stamp?.times(1000)!!))
}

fun Long.toDate(): Date {
    return Date(this)
}
