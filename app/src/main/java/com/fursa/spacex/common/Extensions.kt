package com.fursa.spacex.common

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.app.AlarmManagerCompat
import com.fursa.spacex.presentation.adapters.completed.CompletedModel
import com.fursa.spacex.presentation.adapters.upcoming.UpcomingModel
import com.fursa.spacex.data.network.model.completed.CompletedResponse
import com.fursa.spacex.data.network.model.upcoming.UpcomingResponse
import java.lang.IllegalArgumentException


fun Context.toastMessage(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}


fun List<UpcomingResponse>.mapToUpcoming(): List<UpcomingModel> {
    val result = mutableListOf<UpcomingModel>()
    this.forEach { resp ->
        result.add(UpcomingModel(
            missionName = resp.missionName,
            details = resp.details,
            rocketName = resp.rocket.rocketName,
            flightNumber = resp.flightNumber,
            launchDateUnix = resp.launchDateUnix,
            launchDate = resp.launchDateLocal,
            launchYear = resp.launchYear,
            tbd = resp.tbd))
    }

    return result
}

fun List<CompletedResponse>.mapToCompleted(): List<CompletedModel> {
    val result = mutableListOf<CompletedModel>()
    this.forEach { resp ->
        result.add(CompletedModel(
            missionName = resp.missionName,
            flightNumber = resp.flightNumber,
            launchDate = resp.launchDateUnix,
            videoUrl = resp.links.videoLink,
            result = resp.launchSuccess,
            imageUrl = resp.links.missionPatchSmall))
    }

    return result.reversed()
}

fun String.toMonth(): String {
     return when {
        this.startsWith("янв") -> Month.JAN.monthName
        this.startsWith("фев") -> Month.FEB.monthName
        this.startsWith("мар") -> Month.MAR.monthName
        this.startsWith("апр") -> Month.APR.monthName
        this.startsWith("мая") -> Month.MAY.monthName
        this.startsWith("июн") -> Month.JUN.monthName
        this.startsWith("июл") -> Month.JUL.monthName
        this.startsWith("авг") -> Month.AUG.monthName
        this.startsWith("сен") -> Month.SEP.monthName
        this.startsWith("окт") -> Month.OCT.monthName
        this.startsWith("ноя") -> Month.NOV.monthName
        this.startsWith("дек") -> Month.DEC.monthName
        else -> throw IllegalArgumentException()
    }
}