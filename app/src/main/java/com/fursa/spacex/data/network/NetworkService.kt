package com.fursa.spacex.data.network

import com.fursa.spacex.AppDelegate
import retrofit2.Retrofit
import javax.inject.Inject

const val BASE_URL = "https://api.spacexdata.com/v3/"

class NetworkService {

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var mRetrofit: Retrofit

    fun createSpaceXApi(): SpaceXApiService {
        return mRetrofit.create(SpaceXApiService::class.java)
    }
}