package com.fursa.spacex.data.network.model.mission


data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
)