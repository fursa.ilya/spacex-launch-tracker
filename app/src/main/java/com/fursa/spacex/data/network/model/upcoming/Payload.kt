package com.fursa.spacex.data.network.model.upcoming


import com.google.gson.annotations.SerializedName

data class Payload(
    val customers: List<String>,
    val manufacturer: String,
    val nationality: String,
    @SerializedName("norad_id")
    val noradId: List<Any>,
    val orbit: String,
    @SerializedName("orbit_params")
    val orbitParams: OrbitParams,
    @SerializedName("payload_id")
    val payloadId: String,
    @SerializedName("payload_mass_kg")
    val payloadMassKg: Any,
    @SerializedName("payload_mass_lbs")
    val payloadMassLbs: Any,
    @SerializedName("payload_type")
    val payloadType: String,
    val reused: Boolean,
    val uid: String
)