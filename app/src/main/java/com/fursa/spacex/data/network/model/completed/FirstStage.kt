package com.fursa.spacex.data.network.model.completed


data class FirstStage(
    val cores: List<Core>
)