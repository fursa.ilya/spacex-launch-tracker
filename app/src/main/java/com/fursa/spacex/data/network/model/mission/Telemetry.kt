package com.fursa.spacex.data.network.model.mission


import com.google.gson.annotations.SerializedName

data class Telemetry(
    @SerializedName("flight_club")
    val flightClub: String
)