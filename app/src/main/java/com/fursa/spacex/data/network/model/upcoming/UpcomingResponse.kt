package com.fursa.spacex.data.network.model.upcoming


import com.google.gson.annotations.SerializedName

data class UpcomingResponse(
    val crew: List<Any>,
    val details: String,
    @SerializedName("flight_number")
    val flightNumber: Int,
    @SerializedName("is_tentative")
    val isTentative: Boolean,
    @SerializedName("last_date_update")
    val lastDateUpdate: String,
    @SerializedName("last_ll_launch_date")
    val lastLlLaunchDate: String,
    @SerializedName("last_ll_update")
    val lastLlUpdate: String,
    @SerializedName("last_wiki_launch_date")
    val lastWikiLaunchDate: String,
    @SerializedName("last_wiki_revision")
    val lastWikiRevision: String,
    @SerializedName("last_wiki_update")
    val lastWikiUpdate: String,
    @SerializedName("launch_date_local")
    val launchDateLocal: String,
    @SerializedName("launch_date_source")
    val launchDateSource: String,
    @SerializedName("launch_date_unix")
    val launchDateUnix: Long,
    @SerializedName("launch_date_utc")
    val launchDateUtc: String,
    @SerializedName("launch_site")
    val launchSite: LaunchSite,
    @SerializedName("launch_success")
    val launchSuccess: Any,
    @SerializedName("launch_window")
    val launchWindow: Any,
    @SerializedName("launch_year")
    val launchYear: String,
    val links: Links,
    @SerializedName("mission_id")
    val missionId: List<Any>,
    @SerializedName("mission_name")
    val missionName: String,
    val rocket: Rocket,
    val ships: List<Any>,
    @SerializedName("static_fire_date_unix")
    val staticFireDateUnix: Any,
    @SerializedName("static_fire_date_utc")
    val staticFireDateUtc: Any,
    val tbd: Boolean,
    val telemetry: Telemetry,
    @SerializedName("tentative_max_precision")
    val tentativeMaxPrecision: String,
    val timeline: Any,
    val upcoming: Boolean
)