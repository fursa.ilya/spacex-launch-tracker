package com.fursa.spacex.data.network.model.next


import com.google.gson.annotations.SerializedName

data class Payload(
    @SerializedName("cap_serial")
    val capSerial: Any,
    @SerializedName("cargo_manifest")
    val cargoManifest: Any,
    val customers: List<String>,
    @SerializedName("flight_time_sec")
    val flightTimeSec: Any,
    val manufacturer: String,
    @SerializedName("mass_returned_kg")
    val massReturnedKg: Any,
    @SerializedName("mass_returned_lbs")
    val massReturnedLbs: Any,
    val nationality: String,
    @SerializedName("norad_id")
    val noradId: List<Any>,
    val orbit: String,
    @SerializedName("orbit_params")
    val orbitParams: OrbitParams,
    @SerializedName("payload_id")
    val payloadId: String,
    @SerializedName("payload_mass_kg")
    val payloadMassKg: Any,
    @SerializedName("payload_mass_lbs")
    val payloadMassLbs: Any,
    @SerializedName("payload_type")
    val payloadType: String,
    val reused: Boolean,
    val uid: String
)