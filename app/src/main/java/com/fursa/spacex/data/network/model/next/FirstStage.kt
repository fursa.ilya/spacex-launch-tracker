package com.fursa.spacex.data.network.model.next


import com.google.gson.annotations.SerializedName

data class FirstStage(
    val cores: List<Core>
)