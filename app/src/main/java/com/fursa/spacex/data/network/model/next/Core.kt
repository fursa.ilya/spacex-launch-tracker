package com.fursa.spacex.data.network.model.next


import com.google.gson.annotations.SerializedName

data class Core(
    val block: Int,
    @SerializedName("core_serial")
    val coreSerial: String,
    val flight: Int,
    val gridfins: Any,
    @SerializedName("land_success")
    val landSuccess: Any,
    @SerializedName("landing_intent")
    val landingIntent: Any,
    @SerializedName("landing_type")
    val landingType: Any,
    @SerializedName("landing_vehicle")
    val landingVehicle: Any,
    val legs: Any,
    val reused: Boolean
)