package com.fursa.spacex.data.network.model.upcoming


data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
)