package com.fursa.spacex.data.network.model.completed


data class LaunchFailureDetails(
    val altitude: Int,
    val reason: String,
    val time: Int
)