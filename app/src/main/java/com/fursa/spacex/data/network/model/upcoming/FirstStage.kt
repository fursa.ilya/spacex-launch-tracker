package com.fursa.spacex.data.network.model.upcoming


data class FirstStage(
    val cores: List<Core>
)