package com.fursa.spacex.data.network

import com.fursa.spacex.AppDelegate
import com.fursa.spacex.data.network.model.completed.CompletedResponse
import com.fursa.spacex.data.network.model.mission.MissionResponse
import com.fursa.spacex.data.network.model.next.NextResponse
import com.fursa.spacex.data.network.model.upcoming.UpcomingResponse
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.http.GET
import retrofit2.http.Query


interface SpaceXApiService {

    @GET("launches/upcoming")
    fun getUpcomingAsync(): Deferred<List<UpcomingResponse>>

    @GET("launches/past")
    fun getCompletedAsync(): Deferred<List<CompletedResponse>>

    @GET("launches/next")
    fun getNextLaunchAsync(): Deferred<NextResponse>

    @GET("launches")
    fun getLaunchAsync(@Query("flight_number") flightNumber: Int): Deferred<List<MissionResponse>>

}