package com.fursa.spacex.data.network.model.next


import com.google.gson.annotations.SerializedName

data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
)