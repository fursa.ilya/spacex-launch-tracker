package com.fursa.spacex.data.network.model.completed


data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
)