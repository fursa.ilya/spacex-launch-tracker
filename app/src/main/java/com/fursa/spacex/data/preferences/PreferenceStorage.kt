package com.fursa.spacex.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager


const val KEY_ENABLE_TWITTER = "key_receive_elon_tweets"
const val KEY_APP_THEME= "key_app_theme"
const val KEY_DATA_SOURCE = "key_data_source"

class PreferenceStorage(context: Context) {
    private val appContext = context.applicationContext

    private val preferences: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)

    fun putTwitter(value: Boolean) {
        preferences.edit().putBoolean(KEY_ENABLE_TWITTER, value).apply()
    }

    fun getTwitter() = preferences.getBoolean(KEY_ENABLE_TWITTER, false)

    fun changeTheme() {
        preferences.edit().putBoolean(KEY_APP_THEME, false)
    }

    fun getTheme() = preferences.getBoolean(KEY_APP_THEME, false)

    fun putDataSource() {
        preferences.edit().putBoolean(KEY_DATA_SOURCE, false)
    }

    //false - network, true - local storage
    fun getDataSource() = preferences.getBoolean(KEY_DATA_SOURCE, false)
}