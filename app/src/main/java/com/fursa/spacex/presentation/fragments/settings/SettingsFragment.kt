package com.fursa.spacex.presentation.fragments.settings

import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.fursa.spacex.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.settings)
        val context = preferenceManager.context

        findPreference<Preference>("key_about")?.setOnPreferenceClickListener {
            return@setOnPreferenceClickListener true
        }
    }


}
