package com.fursa.spacex.presentation.fragments.splash

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.fursa.spacex.R
import kotlinx.android.synthetic.main.fragment_splash.*


class SplashFragment : Fragment() {
    private lateinit var upAnim: Animation
    private lateinit var downAnim: Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        upAnim = AnimationUtils.loadAnimation(context, R.anim.uptodown)
        downAnim = AnimationUtils.loadAnimation(context, R.anim.downtoup)
        first.animation = upAnim
        last.animation = downAnim

        button4.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    companion object {
        fun newInstance(): SplashFragment {
            return SplashFragment()
        }
    }
}