package com.fursa.spacex.presentation.fragments.completed

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager

import com.fursa.spacex.R
import com.fursa.spacex.presentation.adapters.completed.CompletedAdapter
import com.fursa.spacex.common.SpaceXListener
import com.fursa.spacex.common.mapToCompleted
import kotlinx.android.synthetic.main.completed_fragment.*

class CompletedFragment : Fragment(), CompletedAdapter.OnCompletedItemClickListener {
    private val completedAdapter = CompletedAdapter(this)
    private lateinit var navController: NavController
    private lateinit var viewModel: CompletedViewModel
    private var onEventsLoadedListener: SpaceXListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CompletedViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is SpaceXListener) {
            onEventsLoadedListener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.completed_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = findNavController()

        with(recyclerCompleted) {
            layoutManager = LinearLayoutManager(context)
            adapter = completedAdapter
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getEvents().observe(this, Observer {
            completedAdapter.setEvents(it.mapToCompleted())
            recyclerCompleted.visibility = View.VISIBLE
            onEventsLoadedListener?.onEventsLoaded()
        })
    }



    override fun onItemSelected(flightNumber: Int) {
        val args = Bundle().apply {
            putInt("flight_number", flightNumber)
        }
        navController.navigate(R.id.launchDetailsFragment, args)
    }

    override fun onDetach() {
        super.onDetach()
        onEventsLoadedListener = null
    }
}
