package com.fursa.spacex.presentation

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.fursa.spacex.AppDelegate
import com.fursa.spacex.R
import com.fursa.spacex.common.SpaceXListener
import com.fursa.spacex.presentation.fragments.splash.SplashFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), SpaceXListener {

    init {
        AppDelegate.appComponent.inject(this)
    }

    private lateinit var navController: NavController

    @Inject
    lateinit var settings: SharedPreferences

    private val allowSplash = settings.getBoolean("key_splash", true)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(allowSplash) {
            Handler().post {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.parent, SplashFragment.newInstance())
                    .addToBackStack(null)
                    .commit()
            }
        }


        navController = findNavController(R.id.nav_host_fragment)
        bottomNavigationView.setupWithNavController(navController)

        bottomNavigationView.setOnNavigationItemSelectedListener(object: BottomNavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                when(menuItem.itemId) {
                    R.id.item_upcoming -> {
                        navController.navigate(R.id.upcomingFragment)
                        progressBar.visibility = View.VISIBLE
                        return true
                    }

                    R.id.item_completed -> {
                        navController.navigate(R.id.completedFragment)
                        progressBar.visibility = View.VISIBLE
                        return true
                    }

                    R.id.item_settings -> {
                        navController.navigate(R.id.settingsFragment)
                        progressBar.visibility = View.GONE
                        return true
                    }

                }

                return true
            }

        })
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onEventsLoaded() {
        progressBar.visibility = View.GONE
    }

}

