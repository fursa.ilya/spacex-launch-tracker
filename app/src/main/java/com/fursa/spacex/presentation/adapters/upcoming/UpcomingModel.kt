package com.fursa.spacex.presentation.adapters.upcoming

data class UpcomingModel(
    val missionName: String?,
    val details: String?,
    val rocketName: String?,
    val flightNumber: Int?,
    val launchDateUnix: Long?,
    val launchDate: String?,
    val launchYear: String,
    val tbd: Boolean //устанавливается в false когда дата известна
)