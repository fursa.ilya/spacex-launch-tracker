package com.fursa.spacex.presentation.fragments.upcoming

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fursa.spacex.AppDelegate
import com.fursa.spacex.data.network.SpaceXApiService
import com.fursa.spacex.data.network.model.upcoming.UpcomingResponse
import kotlinx.coroutines.*
import javax.inject.Inject

class UpcomingViewModel : ViewModel() {

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var apiService: SpaceXApiService

    private val viewModelJob = SupervisorJob()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    private val mutableEvents: MutableLiveData<List<UpcomingResponse>> = MutableLiveData()

    private suspend fun getEventsFromNetwork(): List<UpcomingResponse> {
        return apiService.getUpcomingAsync().await()
    }

    fun getEvents(): MutableLiveData<List<UpcomingResponse>> {
        uiScope.launch {
            val result = getEventsFromNetwork()
            mutableEvents.value = result
        }

        return mutableEvents
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
        uiScope.cancel()
    }
}
