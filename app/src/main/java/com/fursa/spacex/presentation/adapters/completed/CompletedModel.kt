package com.fursa.spacex.presentation.adapters.completed

data class CompletedModel(
    val missionName: String?,
    val flightNumber: Int?,
    val launchDate: Long?,
    val videoUrl: String?,
    val result: Boolean?,
    val imageUrl: String
)