package com.fursa.spacex.presentation.fragments.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.fursa.spacex.AppDelegate
import com.fursa.spacex.R
import com.fursa.spacex.presentation.MainActivity
import com.fursa.spacex.common.toastMessage
import kotlinx.android.synthetic.main.mission_details_fragment.*

class LaunchDetailsFragment : Fragment() {

    init {
        AppDelegate.appComponent.inject(this)
    }

    private var youtubeVideoUrl = ""
    private lateinit var viewModel: MissionDetailsViewModel


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.details_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_watch_on_youtube -> {
                val youtubeIntent = Intent(Intent.ACTION_VIEW, Uri.parse(youtubeVideoUrl))
                startActivity(youtubeIntent)
            }

            R.id.action_share -> {
                context?.toastMessage("Share action selected")
            }
        }

        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.mission_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as MainActivity).setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { findNavController().navigate(R.id.completedFragment) }

        viewModel = ViewModelProviders.of(this).get(MissionDetailsViewModel::class.java)
        
    }
}
