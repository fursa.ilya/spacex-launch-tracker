package com.fursa.spacex.presentation.fragments.upcoming

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.provider.CalendarContract
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.fursa.spacex.AppDelegate
import com.fursa.spacex.R

import com.fursa.spacex.presentation.adapters.upcoming.UpcomingAdapter
import com.fursa.spacex.presentation.adapters.upcoming.UpcomingModel
import com.fursa.spacex.common.SpaceXListener
import com.fursa.spacex.common.formatDate
import com.fursa.spacex.common.mapToUpcoming
import kotlinx.android.synthetic.main.upcoming_fragment.*
import javax.inject.Inject


class UpcomingFragment : Fragment(), UpcomingAdapter.OnAddEventListener {
    private var upcomingAdapter: UpcomingAdapter = UpcomingAdapter()
    private lateinit var viewModel: UpcomingViewModel
    private var onEventsLoadedListener: SpaceXListener? = null

    init {
        AppDelegate.appComponent.inject(this)
    }

    @Inject
    lateinit var settings: SharedPreferences


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is SpaceXListener) {
            onEventsLoadedListener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.upcoming_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(recyclerUpcoming) {
            layoutManager = LinearLayoutManager(context)
            adapter = upcomingAdapter
        }

        upcomingAdapter.setListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UpcomingViewModel::class.java)
        viewModel.getEvents().observe(this, Observer { events ->
            upcomingAdapter.setEvents(events.mapToUpcoming())
            recyclerUpcoming.visibility = View.VISIBLE
            onEventsLoadedListener?.onEventsLoaded()

            events.forEach { launch ->
                if(!launch.tbd) {
                    Log.d("Upcoming", "${launch.missionName} ${formatDate(launch.launchDateUnix)}")
                }
            }

        })
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onAddEventToCalendar(upcoming: UpcomingModel) {
        val calendarIntent = Intent(Intent.ACTION_EDIT)
        calendarIntent.type = "vnd.android.cursor.item/event"
        calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, upcoming.launchDateUnix)
        calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, upcoming.launchDateUnix)
        calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true)
        calendarIntent.putExtra(CalendarContract.Events.TITLE, upcoming.missionName)
        calendarIntent.putExtra(CalendarContract.Events.DESCRIPTION, upcoming.details)
        calendarIntent.putExtra(CalendarContract.Events.RRULE, "FREQ=DAILY")
        startActivity(calendarIntent)

    }

    override fun onDetach() {
        super.onDetach()
        onEventsLoadedListener = null
    }



}
