package com.fursa.spacex.presentation.fragments.completed

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fursa.spacex.AppDelegate
import com.fursa.spacex.data.network.NetworkService
import com.fursa.spacex.data.network.SpaceXApiService
import com.fursa.spacex.data.network.model.completed.CompletedResponse
import kotlinx.coroutines.*
import javax.inject.Inject

class CompletedViewModel : ViewModel() {

    init {
        AppDelegate.appComponent.inject(this)
    }

   private val viewModelJob = SupervisorJob()
   private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)
   private val mutableEvents: MutableLiveData<List<CompletedResponse>> = MutableLiveData()

   @Inject
   lateinit var apiService: SpaceXApiService

   private suspend fun getEventsFromNetwork(): List<CompletedResponse> {
       return apiService.getCompletedAsync().await()
   }

   fun getEvents(): MutableLiveData<List<CompletedResponse>> {
       uiScope.launch {
           val result = getEventsFromNetwork()
           mutableEvents.value = result
       }

       return mutableEvents
   }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
        uiScope.cancel()
    }
}
