package com.fursa.spacex.presentation.adapters.upcoming

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.fursa.spacex.R
import com.fursa.spacex.common.formatDate
import com.fursa.spacex.common.formatUnknownDate
import com.fursa.spacex.common.toMonth

class UpcomingAdapter : RecyclerView.Adapter<UpcomingAdapter.MissionHolder>() {
    private val events = mutableListOf<UpcomingModel>()
    private var listener: OnAddEventListener? = null

    fun setEvents(events: List<UpcomingModel>) {
        this.events.clear()
        this.events.addAll(events)
        notifyDataSetChanged()
    }

    fun setListener(listener: OnAddEventListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MissionHolder {
        val eventView = LayoutInflater.from(parent.context).inflate(R.layout.item_upcoming, parent, false)
        return MissionHolder(eventView)
    }

    override fun getItemCount() = events.count()

    override fun onBindViewHolder(holder: MissionHolder, position: Int) {
        val upcoming = events[position]
        holder.bind(upcoming, listener!!)
    }


    class MissionHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private val missionContainer = itemView.findViewById<ConstraintLayout>(R.id.missionContainer)
        private val txtFlightNumber = missionContainer.findViewById<TextView>(R.id.txtFlightNumber)
        private val txtMissionName = missionContainer.findViewById<TextView>(R.id.txtMissionName)
        private val txtLaunchDate = missionContainer.findViewById<TextView>(R.id.txtLaunchDate)
        private val txtAddSchedule = missionContainer.findViewById<TextView>(R.id.txtSchedule)

        fun bind(upcoming: UpcomingModel, listener: OnAddEventListener) {
            txtFlightNumber.text = "F-NR ${upcoming.flightNumber.toString()}"
            txtMissionName.text = upcoming.missionName
            if(!upcoming.tbd) {
                txtLaunchDate.text = "${formatDate(upcoming.launchDateUnix)}"
            } else {
                txtLaunchDate.text = "${formatUnknownDate(upcoming.launchDateUnix).toMonth()} ${upcoming.launchYear} (TBD)"

            }

            txtAddSchedule.setOnClickListener {
                listener.onAddEventToCalendar(upcoming)
            }
        }
    }

    interface OnAddEventListener {
        fun onAddEventToCalendar(upcoming: UpcomingModel)
    }

}