package com.fursa.spacex.presentation.adapters.completed

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.fursa.spacex.R
import com.fursa.spacex.common.formatDate

class CompletedAdapter(private val listener: OnCompletedItemClickListener) : RecyclerView.Adapter<CompletedAdapter.CompletedViewHolder>() {
    private val events = mutableListOf<CompletedModel>()

    fun setEvents(events: List<CompletedModel>) {
        this.events.clear()
        this.events.addAll(events)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompletedViewHolder {
        val completedView = LayoutInflater.from(parent.context).inflate(R.layout.item_completed, parent, false)
        return CompletedViewHolder(completedView)
    }

    override fun getItemCount() = events.count()

    override fun onBindViewHolder(holder: CompletedViewHolder, position: Int) {
        val completedModel = events[position]
        holder.bind(completedModel, listener)
    }

    class CompletedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val missionContainer = itemView.findViewById<ConstraintLayout>(R.id.missionContainer)

        private val txtFlightNumber = missionContainer.findViewById<TextView>(R.id.txtFlightNumber)
        private val txtMissionName = missionContainer.findViewById<TextView>(R.id.txtMissionName)
        private val txtLaunchDate = missionContainer.findViewById<TextView>(R.id.txtLaunchDate)
        private val txtYoutube = missionContainer.findViewById<TextView>(R.id.txtYoutube)
        private val imageResult = missionContainer.findViewById<ImageView>(R.id.imageResult)
        private val imageMissionPatch = missionContainer.findViewById<ImageView>(R.id.imageMissionPatch)

        fun bind(completedModel: CompletedModel, listener: OnCompletedItemClickListener) {
            txtFlightNumber.text = "F-NR ${completedModel.flightNumber.toString()}"
            txtMissionName.text = completedModel.missionName.toString()
            txtLaunchDate.text = formatDate(completedModel.launchDate)
            if(completedModel.result == true) {
                imageResult.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.success_bullet))
            } else {
                imageResult.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.failure_bullet))
            }
            Glide.with(itemView.context)
                .load(completedModel.imageUrl)
                .into(imageMissionPatch)

            txtYoutube.setOnClickListener {
                val youtubeIntent = Intent(Intent.ACTION_VIEW, Uri.parse(completedModel.videoUrl))
                itemView.context.startActivity(youtubeIntent)
            }

            missionContainer.setOnClickListener { listener.onItemSelected(completedModel.flightNumber!!.toInt()) }
        }

    }

    interface OnCompletedItemClickListener {
        fun onItemSelected(flightNumber: Int)
    }

}