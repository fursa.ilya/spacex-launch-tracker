package com.fursa.spacex

import android.app.Application
import com.fursa.spacex.di.*
import com.fursa.spacex.di.component.AppComponent
import com.fursa.spacex.di.component.DaggerAppComponent
import com.fursa.spacex.di.module.ManagerModule
import com.fursa.spacex.di.module.NetworkModule
import com.fursa.spacex.di.module.SettingsModule


class AppDelegate : Application() {

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .networkModule(NetworkModule())
            .settingsModule(SettingsModule(this))
            .managerModule(ManagerModule(this))
            .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }


}