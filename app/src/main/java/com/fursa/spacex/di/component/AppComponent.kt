package com.fursa.spacex.di.component

import com.fursa.spacex.common.receivers.BootCompleteReceiver
import com.fursa.spacex.data.network.NetworkService
import com.fursa.spacex.data.network.SpaceXApiService
import com.fursa.spacex.di.module.ManagerModule
import com.fursa.spacex.di.module.NetworkModule
import com.fursa.spacex.di.module.SettingsModule
import com.fursa.spacex.presentation.MainActivity
import com.fursa.spacex.presentation.fragments.completed.CompletedFragment
import com.fursa.spacex.presentation.fragments.completed.CompletedViewModel
import com.fursa.spacex.presentation.fragments.details.LaunchDetailsFragment
import com.fursa.spacex.presentation.fragments.upcoming.UpcomingFragment
import com.fursa.spacex.presentation.fragments.upcoming.UpcomingViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        SettingsModule::class,
        ManagerModule::class]
)

interface AppComponent {
    fun inject(apiService: SpaceXApiService)
    fun inject(networkService: NetworkService)
    //viewModels
    fun inject(viewModel: CompletedViewModel)

    fun inject(viewModel: UpcomingViewModel)
    //Fragments
    fun inject(fragment: LaunchDetailsFragment)

    fun inject(fragment: CompletedFragment)
    fun inject(fragment: UpcomingFragment)
    //Activities
    fun inject(activity: MainActivity)
    //Other
    fun inject(bootReceiver: BootCompleteReceiver)
}