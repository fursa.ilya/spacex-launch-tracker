package com.fursa.spacex.di.module

import android.app.AlarmManager
import android.app.Application
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ManagerModule(app: Application) {
    private val appContext = app.applicationContext

    @Singleton
    @Provides
    fun provideNotificationManager() = appContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    @Singleton
    @Provides
    fun provideAppContext() = appContext

    //Todo provideNotificationManager when it will be ready
   /* @RequiresApi(Build.VERSION_CODES.O)
    @Singleton
    @Provides
    fun provideNotificationHelper() = NotificationHelper(appContext.applicationContext)*/
}