package com.fursa.spacex.di.module

import android.app.Application
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SettingsModule(app: Application) {

    private val appContext = app.applicationContext

    @Singleton
    @Provides
    fun providePreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(appContext)
    }

}